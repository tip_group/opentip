# Generated by Django 2.1.5 on 2019-03-25 17:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tip', '0004_auto_20190109_2229'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='url',
            name='added_on',
        ),
        migrations.AddField(
            model_name='url',
            name='isLegitimate',
            field=models.BooleanField(default=False),
        ),
    ]
