# Generated by Django 2.1.5 on 2019-01-09 22:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tip', '0003_auto_20190109_1248'),
    ]

    operations = [
        migrations.AlterField(
            model_name='probe',
            name='apiKey',
            field=models.CharField(max_length=40, unique=True, verbose_name='API Key'),
        ),
    ]
