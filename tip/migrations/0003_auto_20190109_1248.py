# Generated by Django 2.1.5 on 2019-01-09 12:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tip', '0002_auto_20190108_2225'),
    ]

    operations = [
        migrations.CreateModel(
            name='URL',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField(unique=True)),
                ('added_on', models.DateField(auto_now=True)),
            ],
        ),
        migrations.AlterField(
            model_name='probe',
            name='apiKey',
            field=models.CharField(max_length=255, verbose_name='API Key'),
        ),
    ]
