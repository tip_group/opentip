from django.urls import path


from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('api/', views.get_api_response, name='api'),
    path('api/<str:action>', views.get_api_response_action, name='api_action'),
    path('probes/', views.get_probe_answer, name='probes'),
    path('sources/', views.get_source_answer, name='sources'),
    path('events/', views.get_events_answer, name='events'),
    path('login/', views.login_answer, name='login'),
    path('logout/', views.logout_answer, name='logout'),
    path('wiki/', views.wiki_answer, name='wiki')
]
