class ErrorCode:
    ERROR_NO_ERROR = 0, "No error"
    ERROR_INVALID_APIKEY = 1, "Invalid API Key Error"
    ERROR_INVALID_REQUEST = 2, "Invalid request error"
    ERROR_MISSING_ATRIBUTES = 3, "Missing attributes error"
    ERROR_NOT_FOUND = 4, "Not found error"
    ERROR_DATABASE_CONNECTION_ERROR = 5, "Database connection error"
