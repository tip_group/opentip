import datetime
import re


class Event:

    def __init__(self, is_form=False, formFields=None, url=None, isHTTP=True,
                 issuerCompagny=None, ipAddress=None,
                 pageTitle=None, relevant_kee_words=None, registrarDate=None, expirationDate=None,
                 last_update_date=None,
                 domainName=None, nameServers=None):

        self.id = None
        self.domainName = domainName
        self.nameServers = nameServers
        self.whoisServer = None
        self.isForm = is_form
        self.formFields = formFields
        self.registrarDate = registrarDate
        self.expirationDate = expirationDate
        self.lastUpdateDate = last_update_date
        self.url = url
        self.isHTTP = isHTTP
        self.issuerCompagny = issuerCompagny
        self.ipAddress = ipAddress
        self.pageTitle = pageTitle
        self.relevantKeeWords = relevant_kee_words
        self.eventDate = datetime.datetime.now().strftime("%Y-%m-%d %X")
        self.hasBeenAnalyzed = False
        self.organisation = None
        self.dnssec = None
        self.address = None
        self.country = None
        self.state = None
        self.zipcode = None
        self.city = None
        self.cert_issuer = None
        self.referral_url = None
        self.state = None
        self.emails = None
        self.name= None
        self.state = None
        self.is_ML_phishing = False
        self.is_user_phishing = False

    def set_attributes_from_dict(self, dict):
        if 'isForm' in dict:
            if dict['isForm'] == 'true':
                self.isForm = True
            else:
                self.isForm = False

        if 'formFields' in dict:
            self.formFields = dict['formFields']

        if 'registrarDate' in dict:
            self.registrarDate = dict['registrarDate']

        if 'expirationDate' in dict:
            self.expirationDate = dict['expirationDate']

        if 'lastUpdateDate' in dict:
            self.lastUpdateDate = dict['lastUpdateDate']

        if 'url' in dict:
            self.url = dict['url']

        if 'issuerCompagny' in dict:
            self.issuerCompagny = dict['issuerCompagny']

        if 'ipAddress' in dict:
            self.ipAddress = dict['ipAddress']

        if 'pageTitle' in dict:
            self.pageTitle = dict['pageTitle']

        if 'relevantKeeWords' in dict:
            self.relevantKeeWords = dict['relevantKeeWords']

        if 'domainName' in dict:
            self.domainName = dict['domainName']

        if 'nameServers' in dict:
            self.nameServers = dict['nameServers']

        if 'whoisServer' in dict:
            self.whoisServer = dict['whoisServer']

        if 'status' in dict:
            self.status = dict['status']

        if 'isHTTP' in dict:
            if dict['isHTTP'] == "True":
                self.isHTTP = True
            else:
                self.isHTTP = False

        if 'organisation' in dict:
            self.organisation = dict['organisation']

        if 'address' in dict:
            self.address = dict['address']

        if 'city' in dict:
            self.city = dict['city']

        if 'state' in dict:
            self.state = dict['state']

        if 'zipcode' in dict:
            self.zipcode = dict['zipcode']

        if 'country' in dict:
            self.country = dict['country']

        if 'dnssec' in dict:
            self.dnssec = dict['dnssec']

        if 'isPhishing' in dict:
            self.is_phish = dict['isPhishing']

        if 'referral_url' in dict:
            self.referral_url = dict['referral_url']

        if 'certIssuer' in dict:
            self.cert_issuer = dict['certIssuer']

        if 'name' in dict:
            self.name = dict['name']

        if 'emails' in dict:
            self.emails = dict['emails']

        if 'isMLPhishing' in dict:
            if dict['isMLPhishing'] == "True":
                self.is_ML_phishing = True
            else:
                self.is_ML_phishing = False

        if 'isUserPhishing' in dict:
            if dict['isUserPhishing'] == "True":
                self.is_ML_phishing = True
            else:
                self.is_ML_phishing = False

    def bodify(self):
        # Replacing url value by the IP address if the url is None
        body = {
            "domainName": self.domainName,
            "nameServers": self.nameServers,
            "whoisServer": self.whoisServer,
            "status": self.status,
            "isForm": str(self.isForm).lower(),
            'formFields': str(self.formFields),
            'registrarDate': self.registrarDate,
            'expirationDate': self.expirationDate,
            'lastUpdateDate': self.lastUpdateDate,
            'url': self.url if self.url is not None else self.ipAddress,
            'referral_url': self.referral_url,
            'isHTTP': str(self.isHTTP).lower(),
            'organisation': self.organisation,
            'issuerCompagny': self.issuerCompagny,
            'certIssuer': self.cert_issuer,
            'name': self.name,
            'emails': str(self.emails),
            'address': self.address,
            'city': self.city,
            'state': self.state,
            'zipcode': self.zipcode,
            'country': self.country,
            'ipAddress': self.ipAddress,
            'pageTitle': self.pageTitle,
            'revelantKeeWords': self.relevantKeeWords,
            'eventDate': self.eventDate,
            'hasBeenAnalyzed': self.hasBeenAnalyzed,
            'isMLPhishing': self.is_ML_phishing,
            'isUserPhishing': self.is_user_phishing,

        }
        return body

    def registrar_safe(self):
        trusted_reg = ["MarkMonitor, Inc.", "RegistrarSafe, LLC",
                       "CSC CORPORATE DOMAINS, INC.", "RegistrarSafe, LLC", "Eurodns S.A.", "Gabia, Inc.",
                       "Name.com, Inc.", "GoDaddy.com, LLC", "Network Solutions, LLC", "Tucows Domains Inc.",
                       "PortsGroup AB", "GANDI SAS", "eName Technology Co.,Ltd.", "Amazon Registrar, Inc."]
        if self.issuerCompagny in trusted_reg:
            return -1
        elif self.issuerCompagny:
            return 0
        else:
            return 1

    def whois_server(self):
        trusted_whois = ["whois.markmonitor.com", "whois.registrarsafe.com", "whois.corporatedomains.com",
                         "whois.35.com", "grs-whois.hichina.com", "whois.eurodns.com", "whois.gabia.com",
                         "whois.name.com", "whois.godaddy.com", "whois.comlaude.com", "whois.ename.com"]
        if not self.whoisServer:
            return -1
        if self.whoisServer in trusted_whois:
            return -1
        else:
            return 1

    def is_name(self):
        if self.name:
            return -1
        else:
            return 1

    def name_servers(self):
        if not self.nameServers:
            return 1
        array = self.nameServers
        if len(array) > 6:
            return -1
        elif 6 >= len(array) >= 3:
            return 0
        else:
            return 1

    def url_length(self):
        if not self.url:
            return -1
        url = self.url
        length = len(str(url))
        if length < 54:
            return -1
        elif 54 <= length <= 75:
            return 0
        else:
            return 1

    def having_at_symbol(self):
        symbol = re.findall(r'@', self.url)
        if len(symbol) == 0:
            return -1
        else:
            return 1

    def is_cert_clean(self):
        if not self.cert_issuer:
            return -1
        trusted_Auth = ['Comodo', 'Symantec', 'GoDaddy', 'GlobalSign', 'DigiCert', 'StartCom', 'Entrust', 'Verizon',
                        'Trustwave', 'Unizeto', 'Buypass', 'QuoVadis', 'Deutsche Telekom', 'Network Solutions',
                        'SwissSign', 'IdenTrust', 'Secom', 'TWCA', 'GeoTrust', 'Thawte', 'Doster', 'VeriSign']
        if self.cert_issuer in trusted_Auth:
            return 1
        elif self.cert_issuer not in trusted_Auth:
            return 0
        else:
            return -1

    def is_domain_old(self):
        stipped_date = datetime.datetime.strptime(self.registrarDate, '%Y-%m-%d %H:%M:%S')
        current_date = datetime.datetime.now()
        age = (current_date - stipped_date).days
        if age > 360:
            return -1
        elif 360 >= age >= 180:
            return 0
        else:
            return 1

    def is_update(self):
        stipped_date = datetime.datetime.strptime(self.lastUpdateDate, '%Y-%m-%d %H:%M:%S')
        current_date = datetime.datetime.now()
        age = (current_date - stipped_date).days
        if age < 60:
            return -1
        elif 180 <= age <= 60:
            return 0
        else:
            return 1

    def expiration_date(self):
        stipped_date = datetime.datetime.strptime(self.expirationDate, '%Y-%m-%d %H:%M:%S')
        current_date = datetime.datetime.now()
        age = (stipped_date - current_date).days
        if age > 1440:  # 4 years
            return -1
        elif 1440 >= age >= 1080:  # Between 2 and 4 years
            return 0
        else:
            return 1

    def is_organisation(self):
        if self.organisation:
            return -1
        else:
            return 1

    def is_address(self):
        if self.address:
            return -1
        else:
            return 1

    def is_city(self):
        if self.city:
            return -1
        else:
            return 1

    def is_state(self):
        if self.state:
            return -1
        else:
            return 1

    def is_zipcode(self):
        if self.zipcode:
            return -1
        else:
            return 1

    def is_country(self):
        malicious_countries = ['IN', 'ZA', 'SG',
                               'UA', 'ID', 'MY',
                               'RO', 'TR', 'RU']
        if self.country in malicious_countries:
            return 1
        else:
            return -1

    def is_email(self):
        if self.emails:
            return -1
        else:
            return 1

    def is_http(self):
        if not self.isHTTP:
            return -1
        else:
            return 1

    def is_page_title(self):
        if not self.organisation or not self.pageTitle:
            return -1
        if self.pageTitle in self.organisation:
            return -1
        elif self.pageTitle:
            return 0
        else:
            return 1

    def set_ML_phishing(self, val):
        if val in [0,1]:
            if val == 1:
                self.is_ML_phishing = True
            else:
                self.is_ML_phishing = False
            return
        if val not in [False, True]:
            self.is_ML_phishing = False
            return
        self.is_ML_phishing = val

    def getVector(self):

        return [self.registrar_safe(), self.whois_server(), self.is_name(), self.name_servers(), self.url_length(),
                self.having_at_symbol(), self.is_cert_clean(), self.is_domain_old(), self.is_update(),
                self.expiration_date(), self.is_organisation(), self.is_address(), self.is_city(),
                self.is_state(), self.is_zipcode(), self.is_country(), self.is_email(), self.is_http(),
                self.is_page_title()]
