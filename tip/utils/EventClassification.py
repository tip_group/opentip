import joblib
import os


class EventClassification:

    def __init__(self):
        # load the model
        module_dir = os.path.dirname(__file__)  # get current directory
        file_path = os.path.join(module_dir, 'model.pkl')
        self.classifier = joblib.load(file_path)

    def classify_from_vector(self, vector):
        vector = [vector]
        # Making prediction based on the plk model
        prediction = self.classifier.predict(vector)
        return prediction[0]
