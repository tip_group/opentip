#!/bin/python
import requests
import datetime
from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError


es = Elasticsearch({"127.0.0.1:9200"})

try:
    response = es.indices.delete(index='events')
except NotFoundError:
    pass

response = es.indices.create(index="events" ,body={
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "Event" : {
            "properties" : {
                "domainName" : {"type" : "text"},
                "nameServers" : {"type": "text"},
                "whoisServer": {"type": "text"},
                "status": {"type": "text"},
                "isForm": {"type": "boolean"},
                'formFields': {"type": "text"},
                'registrarDate': {"type": "date", "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"},
                'expirationDate': {"type": "date", "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"},
                'lastUpdateDate': {"type": "date", "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"},
                'url': {"type": "text"},
                'referral_url': {"type": "text"},
                'isHTTP': {"type": "boolean"},
                'organisation': {"type": "text"},
                'issuerCompagny': {"type": "text"},
                'certIssuer': {"type": "text"},
                'name': {"type": "text"},
                'emails': {"type": "text"},
                'address' : {"type": "text"},
                "city": {"type": "text"},
                "state": {"type": "text"},
                "zipcode": {"type": "text"},
                "country": {"type": "text"},
                'ipAddress': {"type": "ip"},
                'pageTitle': {"type": "text"},
                'revelantKeeWords': {"type": "text"},
                'eventDate': {"type": "date", "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"},
                'hasBeenAnalyzed': {"type": "boolean"},
                'isMLPhishing' : {"type": "boolean"},
                'isUserPhishing' : {"type": "boolean"}
            }
        }
    }
})
print(response)