import json
from Event import Event


class JsonToCsv:

    @staticmethod
    def vectorToString(vector):
        res = ""
        for item in vector:
            res += (str(item) + ',')
        return res[:-1]

    @staticmethod
    def convert(json_filepath, csv_filename):
        with open(csv_filename, "w+") as csv:
            csv.close()
        csv = open(csv_filename, "a")
        with open(json_filepath, 'r') as file:
            dict = json.load(file)
            for key in dict:
                event = Event()
                event.set_attributes_from_dict(dict[key])
                vector = event.getVector()
                vector = JsonToCsv.vectorToString(vector)
                csv.write(vector + "\n")
                print(vector)

if __name__ == '__main__':
    JsonToCsv.convert("dataset.json", "dataset.csv")
