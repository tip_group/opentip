from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth import authenticate, login, logout

import json
import os

from .controllers.ProbesManager import ProbeManager
from .controllers.EventManager import EventManager
from .utils.ErrorCode import ErrorCode
from .models import URL

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Index page
def index(request):
    # Redirect the client if it's not authenticated
    return HttpResponseRedirect("/login")


# Home page
def home(request):
    return index(request)


def login_answer(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect("/events")

    if request.method == 'GET':
        return render(request, "auth.html", context=None)
    elif request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:  # If user exists, redirect to events page
            login(request, user)
            return HttpResponseRedirect("/events")
        else:
            error = "<div class=\"alert alert-danger\" role=\"alert\">Wrong username or password.</div>"
            context = {'error': error}
            return render(request, "auth.html", context=context)


def logout_answer(request):
    logout(request)
    return HttpResponseRedirect("/")


# View for selecting the API action to perform
# /api/<str:action>
@csrf_exempt
def get_api_response_action(request, action):
    if request.method == 'POST':
        try:
            if request.body:

                body = json.loads(request.body)
                is_auth = ProbeManager.authenticate(body['api_key'])
                if is_auth:
                    # Look action to perform
                    if action == "events":
                        eventManager = EventManager()
                        eventManager.add_event_from_dict(body['event'])
                        return JsonResponse({'error': ErrorCode.ERROR_NO_ERROR})

                    return JsonResponse({'error': ErrorCode.ERROR_MISSING_ATRIBUTES})
                else:
                    return JsonResponse({'error': ErrorCode.ERROR_INVALID_APIKEY})
            else:
                return bad_request()
        except MultiValueDictKeyError as error:
            print(str(error))
            return bad_request()
    elif request.method == 'GET':
        body = json.loads(request.body)
        is_auth = ProbeManager.authenticate(body['api_key'])
        if is_auth:
            if action == "urls":
                urls = ""
                for url in URL.objects.all():
                    urls += url.url + ","
                urls = urls[:len(urls) - 1]
                return HttpResponse(urls)
        else:
            return JsonResponse({'error': ErrorCode.ERROR_INVALID_APIKEY})
    else:
        return HttpResponseBadRequest()


# View used to return wether the api key is good or not
# /api/
@csrf_exempt
def get_api_response(request):
    if request.method == 'POST':
        try:
            body = json.loads(request.body)
            is_auth = ProbeManager.authenticate(body['api_key'])
            if is_auth:
                return JsonResponse({'error': ErrorCode.ERROR_NO_ERROR[0]})
        except MultiValueDictKeyError:
            return bad_request()
        return JsonResponse({'error': ErrorCode.ERROR_INVALID_APIKEY})
    else:
        return bad_request()


# View for getting the probes list or adding a new one
# /probes
def get_probe_answer(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/")

    # Method GET => Getting probes list
    if request.method == 'GET':
        return render(request, "probes.html")

    # Method POST => Adding a new probe
    elif request.method == 'POST':
        is_valid = ProbeManager.validate(request.POST)
        if not is_valid:
            return bad_request()
        if request.POST['action'] == 'get_probes_list':
            probes_list = ProbeManager.get_probes_list()
            return JsonResponse(probes_list, safe=False)
        elif request.POST['action'] == 'add_probe':
            ProbeManager.register_new_probe(request.POST['name'])
            return JsonResponse({'error': ErrorCode.ERROR_NO_ERROR})
        elif request.POST['action'] == 'get_probe_by_id':
            probe = ProbeManager.get_probe_by_id(request.POST['id'])
            if probe is not None:
                return JsonResponse(probe, safe=False)
            else:
                return JsonResponse({'error': ErrorCode.ERROR_NOT_FOUND})
        elif request.POST['action'] == 'renew_api_key':
            id = request.POST['id']
            probe = ProbeManager.get_probe_by_id(id)
            new_key = ProbeManager.generate_key()
            if ProbeManager.update_probe(id, probe['name'], new_key):
                return JsonResponse({'error': ErrorCode.ERROR_NO_ERROR})
            else:
                return JsonResponse({'error': ErrorCode.ERROR_NOT_FOUND})
        elif request.POST['action'] == 'update_name':
            id = request.POST['id']
            name = request.POST['name']
            probe = ProbeManager.get_probe_by_id(id)
            if ProbeManager.update_probe(id, name, probe['apiKey']):
                return JsonResponse({'error': ErrorCode.ERROR_NO_ERROR})
            else:
                return JsonResponse({'error': ErrorCode.ERROR_NOT_FOUND})
        else:
            return JsonResponse({'error': ErrorCode.ERROR_INVALID_REQUEST})


# Function to return bad request code
def bad_request():
    return HttpResponseBadRequest("Bad request")



def get_events_answer(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/")

    if request.method == 'GET':
        return render(request, "events.html")
    if request.method == 'POST':
        if 'action' in request.POST:
            if request.POST['action'] == 'get_event_by_id':
                _id = request.POST['id']
                event_manager = EventManager()
                res = event_manager.get_event_by_id(_id)
                return JsonResponse(res, safe=False)

            elif request.POST['action'] == 'get_all_events':
                event_manager = EventManager()
                if 'page_id' in request.POST:
                    page_id = int(request.POST['page_id'])
                    res = event_manager.get_events(page_id)
                else:
                    res = event_manager.get_events(0)
                print(res)
                return JsonResponse(res, safe=False)

            elif request.POST['action'] == 'qualify_event':
                if 'id' in request.POST:
                    if 'value' in  request.POST:
                        event_manager = EventManager()
                        event_manager.qualify_event(request.POST['id'], request.POST['value'])

        return JsonResponse({'error': "bad request"})

    return bad_request()


def get_source_answer(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/")

    if request.method == 'POST':
        if 'action' in request.POST:
            if request.POST['action'] == 'get_sources_list':
                urls = {}
                for url in URL.objects.all():
                    urls[url.url] = url.isLegitimate
                return JsonResponse(urls, safe=False)
            elif request.POST['action'] == 'delete':
                if 'url' in request.POST:
                    try:
                        url = URL.objects.get(url=request.POST['url'])
                        url.delete()
                    except URL.DoesNotExist:
                        return JsonResponse({'error': ErrorCode.ERROR_NOT_FOUND})
                return JsonResponse({'error': ErrorCode.ERROR_NO_ERROR})
            elif request.POST['action'] == 'add':
                if 'url' in request.POST:
                    if request.POST['url'] != "":
                        legitimate = True if 'legitimate' in request.POST and request.POST[
                            'legitimate'] == 'true' else False
                        url = URL(url=request.POST['url'], isLegitimate=legitimate)
                        url.save()
                        return JsonResponse({'error': ErrorCode.ERROR_NOT_FOUND})
                return JsonResponse({'error': ErrorCode.ERROR_NO_ERROR})

    return bad_request()

def wiki_answer(request):

    if request.method == 'GET':
        return render(request, "wiki.html", context=None)
