from ..models import Probe
import secrets


class ProbeManager:
    ERROR_INVALID_PROBE_NAME = 10

    key_length = 40

    actions = {'get_probes_list',
               'update_probe',
               'delete_probe',
               'add_probe',
               'get_probe_by_id',
               'renew_api_key'}

    @staticmethod
    def get_probes_list():
        values = []
        for value in Probe.objects.all().values():
            values.append(value)
        return values

    @staticmethod
    def get_probe_by_id(probe_id):
        try:
            probe = Probe.objects.get(id=probe_id)
            return probe.values() if probe is not None else None
        except Exception:
            return None

    @staticmethod
    def register_new_probe(name):
        try:
            Probe.objects.get(name=name)
            return False
        except Probe.DoesNotExist:
            # Generation of api key
            key = ProbeManager.generate_key()
            probe = Probe(name=name, apiKey=key)
            probe.save()
            return True

    @staticmethod
    def update_probe(probe_id, name, apikey):
        try:
            probe = Probe.objects.get(id=probe_id)
            probe.name = name
            probe.apiKey = apikey
            probe.save()
            return True
        except Probe.DoesNotExist:
            return False

    @staticmethod
    def generate_key():
        exists = 1
        while exists > 0:
            key = secrets.token_urlsafe(ProbeManager.key_length)
            try:
                exists = Probe.objects.get(apiKey__exact=key).count()
            except Probe.DoesNotExist:
                return key

    @staticmethod
    def delete_probe_by_key(key):
        try:
            probe = Probe.objects.get(apiKey__exact=key)
            probe.delete()
            return True
        except Probe.DoesNotExist:
            return False

    @staticmethod
    def validate(params):
        # to secure for V1t4m1n3
        if 'action' in params:
            if params['action'] in ProbeManager.actions:
                if params['action'] == 'get_probe_by_id':
                    if 'id' not in params:
                        return False
                if params['action'] == 'renew_api_key':
                    if 'id' not in params:
                        return False
                return True

        return False

    @staticmethod
    def authenticate(api_key):
        try:
            probe = Probe.objects.get(apiKey__exact=api_key)
            if probe:
                return True
        except Probe.DoesNotExist:
            return False
        return False
