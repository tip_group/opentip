from elasticsearch import Elasticsearch
from elasticsearch.exceptions import ConnectionError
from tip.utils.ErrorCode import ErrorCode
from tip.utils.Event import Event
from tip.utils.EventClassification import EventClassification

BDD_HOST = '127.0.0.1'
BDD_PORT = '9200'
DOC_TYPE = 'Event'
INDEX_NAME_EVENT = 'events'
SEARCH_TYPE = 'query_then_fetch'
SEARCH_SIZE = 20

SEARCH_BODY_GET_UNANALYZED_EVENT = {
    'query': {
        "bool": {
            'must': {'match': {'hasBeenAnalyzed': 'false'}},
        }
    },
    'sort': {'eventDate': {'order': 'asc'}},
}

SEARCH_BODY_GET_All_EVENT = {
    'query': {
        'match_all': {}
    },
    'sort': {'eventDate': {'order': 'desc'}}
}

# Index creation code - for information purpose
'''
response = es.indices.create(index="events" ,body={
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "Event" : {
            "properties" : {
                "domainName" : {"type" : "text"},
                "nameServers" : {"type": "text"},
                "whoisServer": {"type": "text"},
                "status": {"type": "text"},
                "isForm": {"type": "boolean"},
                'formFields': {"type": "text"},
                'registrarDate': {"type": "date", "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"},
                'expirationDate': {"type": "date", "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"},
                'lastUpdateDate': {"type": "date", "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"},
                'url': {"type": "text"},
                'referral_url': {"type": "text"},
                'isHTTP': {"type": "boolean"},
                'organisation': {"type": "text"},
                'issuerCompagny': {"type": "text"},
                'certIssuer': {"type": "text"},
                'name': {"type": "text"},
                'emails': {"type": "text"},
                'address' : {"type": "text"},
                "city": {"type": "text"},
                "state": {"type": "text"},
                "zipcode": {"type": "text"},
                "country": {"type": "text"},
                'ipAddress': {"type": "ip"},
                'pageTitle': {"type": "text"},
                'revelantKeeWords': {"type": "text"},
                'eventDate': {"type": "date", "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"},
                'hasBeenAnalyzed': {"type": "boolean"},
                'isMLPhishing' : {"type": "integer"},
                'isUserPhishing' : {"type": "integer"}
            }
        }
    }
})
'''


class EventManager:
    database = None
    classifier = None

    def __init__(self):
        self.init_connection()
        self.classifier = EventClassification()

    # Retrieve the earliest event wich hasn't been analyzed yet
    def get_unqualified_event(self):
        event = self.database.search(index=INDEX_NAME_EVENT, doc_type=DOC_TYPE, search_type=SEARCH_TYPE,
                                     body=SEARCH_BODY_GET_UNANALYZED_EVENT, size=1)
        event = event['hits']['hits']
        res = Event()
        res.set_attributes_from_dict(event)
        return res

    def get_events(self, from_):
        try:
            events = self.database.search(index=INDEX_NAME_EVENT, doc_type=DOC_TYPE, search_type=SEARCH_TYPE,
                                          body=SEARCH_BODY_GET_All_EVENT, size=SEARCH_SIZE, from_=(from_ * SEARCH_SIZE))
            return {'Error': ErrorCode.ERROR_NO_ERROR[0],
                    'ErrorText': ErrorCode.ERROR_NO_ERROR[1],
                    'Events': events['hits']['hits']
                    }
        except ConnectionError:
            return {'Error': ErrorCode.ERROR_DATABASE_CONNECTION_ERROR[0],
                    'ErrorText': ErrorCode.ERROR_DATABASE_CONNECTION_ERROR[1]}

    def get_event_by_id(self, _id):
        try:
            event = self.database.get(index=INDEX_NAME_EVENT, id=_id, doc_type=DOC_TYPE)
            return {'Error': ErrorCode.ERROR_NO_ERROR[0],
                    'ErrorText': ErrorCode.ERROR_NO_ERROR[1],
                    'Event': event
                    }
        except ConnectionError:
            return {'Error': ErrorCode.ERROR_DATABASE_CONNECTION_ERROR[0],
                    'ErrorText': ErrorCode.ERROR_DATABASE_CONNECTION_ERROR[1]}

    def qualify_event(self, id, is_user_phishing):
        if int(is_user_phishing) not in [0, 1]:
            return {'Error': ErrorCode.ERROR_MISSING_ATRIBUTES[0],
                    'ErrorText': ErrorCode.ERROR_MISSING_ATRIBUTES[1]}

        event_res = self.get_event_by_id(id)
        if event_res['Error'] == 0:
            event = event_res['Event']['_source']
            event['isUserPhishing'] = True if is_user_phishing == '1' else False
            event['hasBeenAnalyzed'] = True
            self.database.index(index='events', doc_type=DOC_TYPE, id=id, body=event)

        return {'Error': ErrorCode.ERROR_DATABASE_CONNECTION_ERROR[0],
                'ErrorText': ErrorCode.ERROR_DATABASE_CONNECTION_ERROR[1]}

    def get_events_count(self):
        count = self.database.search(index=INDEX_NAME_EVENT, doc_type=DOC_TYPE, search_type=SEARCH_TYPE,
                                     body=SEARCH_BODY_GET_All_EVENT)
        count = count['hits']
        count = count['total']
        return int(count)

    def add_event(self, event: Event):
        count = self.get_events_count(self)
        event.set_attributes_from_dict(dict)
        event.set_ML_phishing(self.classifier.classify_from_vector(event.getVector()))
        if not self.exist(event.url):
            self.database.index(index='events', doc_type=DOC_TYPE, id=count + 1, body=event.bodify())
        return

    def add_event_from_dict(self, dict):
        count = self.get_events_count()
        event = Event()
        event.set_attributes_from_dict(dict)
        event.set_ML_phishing(self.classifier.classify_from_vector(event.getVector()))
        if not self.exist(event.url):
            self.database.index(index='events', doc_type=DOC_TYPE, id=count + 1, body=event.bodify())
        return

    def exist(self, match_url):
        res = self.database.search(index=INDEX_NAME_EVENT, doc_type=DOC_TYPE, search_type=SEARCH_TYPE,
                                   body={
                                       "query": {
                                           "bool": {
                                               "must": {
                                                   "match": {
                                                       "url": match_url
                                                   }
                                               }

                                           }
                                       }
                                   })

        count = res['hits']
        count = count['total']
        if int(count) > 0:
            first_match = res['hits']['hits'][0]
            if first_match['_source']['url'] == match_url:
                return True
        return False

    def init_connection(self):
        # config = Config.objects.get()
        self.database = Elasticsearch({BDD_HOST + ':' + BDD_PORT})
        return
