from django.contrib import admin
from .models import Probe
from .models import URL


# Register your models here.
class ProbeAdmin(admin.ModelAdmin):
    pass


class URLAdmin(admin.ModelAdmin):
    pass


admin.site.register(Probe, ProbeAdmin)
admin.site.register(URL, URLAdmin)
