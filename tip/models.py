from django.db import models
from django.utils.html import escape


# Validators

def validatePort(value):
    if 1 > value and value > 65535:
        raise ValueError(('%(value)s : invalid port number'),
                         params={'value': value})


# Create your models here.
class Probe(models.Model):
    name = models.CharField(max_length=255)
    apiKey = models.CharField("API Key", max_length=40, unique=True)

    def values(self):
        res = {'id': self.id, 'name': self.name, 'apiKey': self.apiKey}
        return res

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.name = escape(self.name)
        super().save(force_insert, force_update, using, update_fields)


class URL(models.Model):
    url = models.URLField(unique=True)
    isLegitimate = models.BooleanField(default=False)

    def dictionnarify(self):
        return {'url': self.url, 'legitimate': self.isLegitimate}


class Config(models.Model):
    elastiSearchAdresse = models.GenericIPAddressField(unique=True)
    elastiSearchPort = models.IntegerField(validators=[validatePort])

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if len(Config.objects.all()) > 1:
            raise Exception("Cannot add more than one configuration.")
        else:
            super().save(force_insert, force_update, using, update_fields)
