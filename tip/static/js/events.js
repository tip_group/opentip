var pageId = 0;

function fillFocusBlock(data){
    console.log(data)

    if(data['Error'] == 0){
        data = data['Event']
        $('#event-focus-default-message').addClass('thin')
        $('#event-focus-default-message').addClass('invisible')
    
        $('#event-focus').addClass('toggle')
        $('#event-focus').removeClass('thin')
        $('#event-focus').removeClass('invisible')
    
        $('#button-toggle-focus-block').removeClass('invisible')
        // Reseting qualification badge
        $('#form-event-qualification').removeClass("badge-danger")
        $('#form-event-qualification').removeClass("badge-secondary")
        $('#form-event-qualification').removeClass("badge-success")
    
        var event_data = data['_source']
        var isMLPhishing = event_data['isMLPhishing']
        var isUserPhishing = event_data['isUserPhishing']
        var hasBeenAnalyzed = event_data['hasBeenAnalyzed']
        // Qualification badge filling
        if(hasBeenAnalyzed)
            if (isUserPhishing){
                $('#form-event-qualification').addClass("badge-danger")
                $('#form-event-qualification').html("Malicious")
            }
            else{
                $('#form-event-qualification').addClass("badge-success")
                $('#form-event-qualification').html("Legitimate")
            }
        else{
            $('#form-event-qualification').addClass("badge-secondary")
            $('#form-event-qualification').html("Not qualified")
        }
        // Filling the rest of the form
        $('#form-event-id').val(data['_id'])
        $('#form-event-date').val(event_data['eventDate'])
        $('#form-event-url').val(event_data['url'])
        $('#form-event-title').val(event_data['pageTitle'])
        $('#form-event-form').val(event_data['isForm'])
        $('#form-event-domain').val(event_data['domainName'])
        $('#form-event-ip').val(event_data['ipAddress'])
        $('#form-event-dns').val(event_data['nameServers'])
        $('#form-event-registrar').val(event_data['organisation'])
        $('#form-event-registration-date').val(event_data['registrarDate'])
        $('#form-event-expiration-date').val(event_data['expirationDate'])
        $('#form-event-update-date').val(event_data['lastUpdateDate'])
        $('#form-event-http').val(event_data['isHTTP'])
        $('#form-event-issuer').val(event_data['issuerCompagny'])
        $('#form-event-emails').val(event_data['emails'])
        $('#form-event-location').val(event_data['address'] + ' - ' + event_data['country'])

        // Machine learning qualification filling
        $('#modal-qualification-badge').removeClass("bagde-success")
        $('#modal-qualification-badge').removeClass("badge-danger")
        if(isMLPhishing){
            $('#modal-qualification-badge').html("Malicious")
            $('#modal-qualification-badge').addClass("badge-danger")
        }
        else{
            $('#modal-qualification-badge').html("Legitimate")
            $('#modal-qualification-badge').addClass("badge-success")
        }
        // Listeners for the qualification modal
        $('#modal-malicious-button').click(function(e){
            $('#event-qualification-modal').modal('hide')
            url = '/events/'
            data = {'action': "qualify_event", 'id': $('#form-event-id').val(), 'value': 1}
            asynCall(url, data, updateList($('#form-event-id').val()));
        });

        $('#modal-legitimate-button').click(function(e){
            $('#event-qualification-modal').modal('hide')
            url = '/events/'
            data = {'action': "qualify_event", 'id': $('#form-event-id').val(), 'value': 0}
            asynCall(url, data, updateList($('#form-event-id').val())); 
        });
            
    }
    else{
        $('#event-focus-default-message').html(data['ErrorText']);
        $('#event-focus-default-message').addClass('.text-danger');
    }

}

function updateList(id){
    var url = '/events/';
    data = {'action' : 'get_all_events'};
    asynCall(url, data, fill_events_list);
    applyFocusById(id)
}

function applyFocus(target){
    //console.log(target.currentTarget.children[0].innerHTML)
    url = '/events/'
    data ={'action' : 'get_event_by_id','id' : target.currentTarget.children[0].innerHTML }
    asynCall(url, data, fillFocusBlock)
   
}

function applyFocusById(id){
    //console.log(target.currentTarget.children[0].innerHTML)
    url = '/events/'
    data ={'action' : 'get_event_by_id','id' : id }
    asynCall(url, data, fillFocusBlock)
}

function fill_events_list(data){
    if(data['Error'] == '0'){
        console.log(data['Events']);
        $('#event-list-body').html("");
        data = data['Events']

        for (var field in data){
            
            event_source = data[field]['_source'];
            id = data[field]['_id']
            badgeType = "primary"
            html = "<tr class\"event-line\"><th scope=\"row\">"+ id +"</th>"
            if(event_source['hasBeenAnalyzed'])
                if(event_source['isUserPhishing'])
                    html += "<td class=\"bg-danger text-white\"><span>Malicious</span></td>"
                else
                    html += "<td class=\"bg-success text-white\"><span>Legitimate</span></td>"

            else
                html += "<td class=\"bg-secondary text-white\"><span>Not qualified</span></td>"

            html += "<td class=\"url-cell\">"+ event_source['url'] +"</td>"
            html += "<td>"+ event_source['ipAddress'] +"</td>"
            html += "<td>"+ event_source['domainName'] +"</td>"
            html += "<td>"+ event_source['eventDate'] +"</td>"
            html += "<td>"+ event_source['registrarDate'] +"</td>"
            html += "<td>"+ event_source['organisation'] +"</td>"
            html += "</tr>"
            $('#event-list-body').append(html)
        }

        $( "tr" ).click(function(e) {
        applyFocus(e)
        });
    }
    else{
        $('#event-focus-default-message').html(data['ErrorText']);
        $('#event-focus-default-message').addClass('.text-danger');
    }
}


$(document).ready(function () {
    var url = '/events/';
    data = {'action' : 'get_all_events'};
    asynCall(url, data, fill_events_list);
    $('#message-block').addClass('thin');

    $(".event-page-link").click(function(e){
        //console.log(e.target)
        ariaLabel = e.target.attributes['aria-label'].value;
        data = {'action' : 'get_all_events'};
        if(ariaLabel == "Previous"){
            if(pageId <= 0){
                data['page_id'] = 0;
            }
            else{
                data['page_id'] = --pageId;
            }
        }
        else if (ariaLabel == "Next"){
            data['page_id'] = ++pageId;
        }
        asynCall(url, data, fill_events_list);
    })

    $('#button-toggle-focus-block').click(function(e){
        //console.log($('#event-focus').css('height'))
        toggleVisible("event-focus")
        toggleThin("event-focus")
        if($('#event-focus').hasClass('toggle')){
            $('#event-focus').removeClass('toggle')
        }
        else {
            $('#event-focus').addClass('toggle')
        }
    })
  });
