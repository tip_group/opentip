function asynCall(url, data, callback){
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        type : 'POST',
        url: url,
        data : data,
        success : callback,
        beforeSend : function(xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    })
}

function toggleVisible(id){
   if ($('#'+id).hasClass( "invisible" )){
    $('#'+id).removeClass( "invisible" )
   }
   else{
    $('#'+id).addClass( "invisible" )
   }
    
}

function toggleThin(id){
    if ($('#'+id).hasClass( "thin" )){
     $('#'+id).removeClass( "thin" )
    }
    else{
     $('#'+id).addClass( "thin" )
    }
     
 }

