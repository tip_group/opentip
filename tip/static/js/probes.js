function reload(){
    location.reload();
}

function update_sources_list() { 
    url = '/sources/'
    data = {'action': 'get_sources_list'}
    asynCall(url, data, fill_sources_list)
 }

 function deleteSource(source) {
     console.log('delete')
    url = '/sources/'
    data = {'action': 'delete', 'url': source}
    asynCall(url, data, update_sources_list)
 }

 function addSource(data){
    url = '/sources/'
    asynCall(url, data, update_sources_list)
 }

async function fill_probes_list(data){
    for(var probe in data){
        probeInfo = "<a href=\"#\" id=\""+data[probe]['id']+"\" class=\"list-group-item active probe-id-item\">"+ data[probe]['id'] +" - " + data[probe]['name'] +"</a>"
        $('#probe-select-id-list').append(probeInfo)
    }

    $('#probe-select-dropdown').prop('disabled', false);
    $('#probe-select-dropdown').text("Select");

    $('.probe-id-item').click(function(e){
        id=e.delegateTarget.id
        var url = '/probes/'
        data = {'action' : 'get_probe_by_id', 'id' : id}
        asynCall(url, data, fill_probes_form)
    })
}

async function fill_sources_list(data) { 
    legitimateList = $('#legitimate-url-list')
    uncategorizedList = $('#uncategorized-url-list')
    legitimateList.html("")
    uncategorizedList.html("")
    for(url in data){
        html = "<li class=\"list-group-item\">"
        html += url
        html += "<button class=\"btn btn-sm btn-danger float-right button-delete-source\">X</button></li>"
        if(data[url])
            legitimateList.append(html)

        else if (!data[url])
            uncategorizedList.append(html)
    }

    $('.button-delete-source').click(function (e) { 
        source = e.target.parentElement.innerHTML.split('<')[0]
        deleteSource(source)
     })
 }

async function fill_probes_form(data){
    $("#form-probe-id").val(data['id'])
    $("#form-probe-name").val(data['name'])
    $("#form-probe-apikey").val(data['apiKey'])

    $("#probe-manage-form").attr("newentry", false)

    $("#probe-select-message").addClass('invisible')
    $("#probe-select-message").addClass('thin')

    $("#probe-manage-form").removeClass('invisible')
    $("#probe-manage-form").removeClass('thin')

    $("#modal-confirm-button").click(function(e){
        $('.modal').modal('hide')
        asynCall("/probes/", {'action': "renew_api_key", 'id': $("#form-probe-id").val()}, function(e){
            $('#' + $("#form-probe-id").val()).click()
        })
    });

}

$(document).ready(function () {
    url = '/probes/'
    data = {'action': 'get_probes_list'}
    asynCall(url, data, fill_probes_list)

    $('#add-probe-button').click(function(e){

        $("#probe-manage-form").attr("newentry", true)
        $("#probe-select-message").addClass('invisible')
        $("#probe-select-message").addClass('thin')

        $("#probe-manage-form").removeClass('invisible')
        $("#probe-manage-form").removeClass('thin')
        $("#form-probe-id").val("")
        $("#form-probe-apikey").val("")
        $("#form-probe-name").val("")
    })

    $("#btn-submit").click(function(e){
        url = '/probes/'
        data = null
        name = $("#form-probe-name").val()
        id = $("#form-probe-id").val()
        newentry = $("#probe-manage-form").attr("newentry")
        if (newentry == 'true'){
            data = {'action': 'add_probe', 'name': name}
            asynCall(url, data, reload)
        }
        else if (newentry == 'false'){
            data = {'action': 'update_name', 'name': name, 'id': id}        
            asynCall(url, data, )
            $('#' + $("#form-probe-id").val()).click()
        }
        
    })

    $("#btn-reset").click(function(e){
        newentry = $("#probe-manage-form").attr("newentry")
        if (newentry == 'true'){
            $("#form-probe-id").val("")
            $("#form-probe-apikey").val("")
            $("#form-probe-name").val("")
        }
        else if (newentry == 'false') {
            $('#' + $("#form-probe-id").val()).click()
        }
    })

    update_sources_list()

    $('.btn').click(function(e){
        id = e.target.id
        //console.log(id)
        data = {'action': 'add'}
        if (id == "button-add-uncategorized-url"){
            data['url'] = $('#form-uncategorized-url-name').val()
            data['legitimate'] = "false"
            addSource(data)
            $('#form-uncategorized-url-name').val("")
            
        }
        else if (id == "button-add-legitimate-url"){
            data['url'] = $('#form-legitimate-url-name').val()
            data['legitimate'] = "true"
            addSource(data)
            $('#form-legitimate-url-name').val("")
        }
    })

  });